# OptimizeMe

Application optimizes all pictures within a directory the code is placed. It uses Tinypng API.

INSTALL

1. Clone repository.
2. Put files in directory you want to optimize.
3. Install dependencies - npm install
4. Install glob - npm install glob
5. Run script - node script.js

Warning - this script is creating new optimized files in the place of unoptimized.
The old ones will be removed. If you want to save unoptimized pictures, please create a copy.
