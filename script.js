var tinify = require("tinify");
var fs = require("fs");
var glob = require("glob")
tinify.key = "LqHPwC8DSgFeFV7FAjm-GyVCla1y8OFf";

// Create pattern for all directories and subdirectories

function getPaths(src, callback) {
    glob(src + '/**/*', callback);
};

// Get all paths and files in main directory
getPaths('./', function(err, res) {
    if (err) {
        console.log('Sorry, we have an error: ', err);
    } else {
        res.forEach(path => {

            // Skip node modules and script files
            if (!(path.substring(0, 14) == "./node_modules") &&
                !(path === "./package.json") &&
                !(path === "./README.md") &&
                !(path === "./script.js")) {
                // Optimize pictures (API is useful only in .jpg, .jpeg and .png formats)
                console.log("Application will process: ", path);
                var source = tinify.fromFile(path);
                // Delete old one, create optimized picture in the same place
                source.toFile(path)
            }
        })
    }
});
